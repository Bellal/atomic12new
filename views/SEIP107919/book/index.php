<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\book\book;

$book = new Book();
$var =$book->index();

?>
<html>
    <head>
     
        <title>Book Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Book List</h1>
            </center>
        </header>
        <hr>
     <center>
             
   <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                    
  <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th>Serial</th>
        <th>Name</th>
        <th>Author</th>
        <th>Action</th>
      </tr>
    </thead>
            
           <?php foreach($var as $book): ?>
           <tr class="info"><td><?php echo $book['id'];?></td><td><?php echo $book['title'];?></td><td><?php echo $book['author'];?></td><td><a href="view.php">View</a> | <a href="edit.php">Edit</a> | <a href="delete.php">Delete</a></td></tr>
            <?php endforeach;?>
 </table>
            
       </center>
 </div> 
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
