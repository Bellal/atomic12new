<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\hobby\Hobby;

$hobby = new Hobby();
$var =$hobby->index();

?>




<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Hobby List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Hobby List</h1>
            </center>
        </header>
        <hr>
        
        
            <center>
  <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                    
 <table class="table table-bordered">
                               <tr class="success"><th width="50">Serial</th><th>Person Name</th><th>Hobby</th><th>Action</th></tr>
                               <?php foreach($var as $hobby): ?>
                               <tr><td><?php echo $hobby['id'];?></td><td><?php echo $hobby['name'];?></td>
                                   <td>
                                       <?php $z=unserialize($hobby['hobby']);
                                        foreach ($z as $contractor)
                                        echo htmlspecialchars($contractor).'<br/>';
                                        ?>
                   </td>
                                     <td width="190">
                                         <div class="btn-group">
                                            <a class="btn btn-primary" href="view.php">View</a>
                                            <a class="btn btn-success" href="edit.php">Edit</a>
                                            <a class="btn btn-danger" href="delete.php">Delete</a>
                                         </div>
                                     </td>
                               </tr>
                   <?php endforeach;?>
                
  </table>
  </center>
  </div>
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
