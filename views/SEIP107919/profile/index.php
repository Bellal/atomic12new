<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\profile\Profile;

$profile = new Profile();
$profiles=$profile->index();

?>




<!DOCTYPE html>
<html>
    <head>
       
        <title>Profile Information</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
       
        
       <div class="col-sm-2">
                
            </div>
            <center>
             
                   
                    
                    
                <div id="loginbox" style="margin-top:20px;" class="mainbox col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-primary"> 
                        <div class="panel-heading">
                            
                            <ul class="pager">
                                <h1>Profile Information</h1>
                                <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                                <li class="previous"></li>   
                                <li class="next"><a href="create.php">Create New</a></li>
                           </ul>
                            
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                               <tr class="success"><th width="50">Serial</th><th>Name</th><th>Image</th><th>Action</th></tr>
                               <?php foreach($profiles as $profile): ?>
                               <tr><td><?php echo $profile['id'];?></td><td><?php echo $profile['name'];?></td>
                                   <td width="100"> <?php echo "<img class='img-rounded' alt='Cinque Terre' width='80' height='50' src='userphoto/".$profile['image']."'  />"; ?>
                                   </td>
                                     <td width="190">
                                         <div class="btn-group">
                                            <a class="btn btn-primary" href="view.php">View</a>
                                            <a class="btn btn-success" href="edit.php">Edit</a>
                                            <a class="btn btn-danger" href="delete.php">Delete</a>
                                         </div>
                                     </td>
                               </tr>
                              <?php endforeach; ?>




                            </table>
                    </div>
                </div>
            </div>
            
        </div>
                
                
               
            </center>
        </div>
    </body>
</html>
