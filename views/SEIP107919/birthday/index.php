<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."ATOMIC12".DIRECTORY_SEPARATOR."vendor/autoload.php");
use ATOMIC12\BITM\seip107919\birthday\Birthday;
use ATOMIC12\BITM\seip107919\Message\message;
use ATOMIC12\BITM\seip107919\Utility\Utility;

$birthday=new Birthday();
$var = $birthday->index();

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Birthday List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
   <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Birthday List</h1>
            </center>
        </header>
        <hr>
        
       
            <center>
             
  <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                    
                         <div>
                            <?php echo Message::flash();?>.
                          </div>          
 <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th>Serial</th>
        <th>Name</th>
        <th>Birthday</th>
        <th>Action</th>
      </tr>
    </thead>
                   
                   <?php foreach($var as $birthday): ?>
                   <tr class="info"><td>
                       <?php echo $birthday['id'];?></td>
                       <td><?php echo $birthday['name'];?></td><td><?php echo $birthday['birthday'];?></td>
                        
                       <td><a class="btn btn-success"href="view.php?id=<?php echo $birthday['id'];?>">View</a>  <a class="btn btn-success" href="edit.php?id=<?php echo $birthday['id'];?>">Edit</a><a class="btn btn-success"href="delete.php?id=<?php echo $birthday['id'];?>">Delete</a>
                   
                          </td>
                          </tr>
                          
                          
                                            
                                       
                       <?php endforeach;?>
                   
                   
                
                
                
                </table>
         </center>
     
    </div>    
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script>
            $(document).ready(function(){
                 $('#delete').bind('click',function(e){
                  
                    var isOk = confirm("Are you sure you want to delete?");
                    //console.log(isOk);
                    if(!isOk){
                        e.preventDefault();
                    }
             });
                
            });
           
        </script>
    </body>
</html>
