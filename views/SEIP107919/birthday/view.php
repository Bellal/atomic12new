<?php

session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\birthday\Birthday;

$birthday = new Birthday();
$var =$birthday->view($_GET['id']);

?>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Birthday Information</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
    </head>
    <body>
        <div class="container">
            
                        <div class="panel-heading"><ul class="pager">
                            <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                            <li class="next"><a href="create.php">Create New</a></li>Birthday Information
                          </ul>
                        </div>
                        
                            <table class="table table-bordered">
                                 <tr class="success"><th width="50"><h4>ID</h4></th><th><h4>Name</h4></th><th></h4>Birthday</h4></th><th><h4>Action</h4></th></tr>
                                 
                                 <tr><td><h4><?php echo $var->id ;?></h4></td><td><h4><?php echo $var->name;?></h4></td><td><h4><?php echo $var->birthday;?></h4></td>
                                       <td width="150">
                                        <a class="btn btn-info" href="edit.php?id=<?php echo $var->id;?>">Edit</a>
                                         <a class="btn btn-info" href="delete.php?id=<?php echo $var->id;?>">Delete</a>
                                        
                                       </td>
                                 </tr>
                         </table>
                        </div>
                    
                             
       
        <script src="../js/bootstrap.js"></script>
        <script src="../jslib/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#delete').bind('click',function(e){
                  
                    var isOk = confirm("Are you sure you want to delete?");
                    //console.log(isOk);
                    if(!isOk){
                        e.preventDefault();
                    }
             });
                
            });
           
        </script>
    </body>
</html>