<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\birthday\birthday;
use ATOMIC12\BITM\seip107919\Message\Message;
use ATOMIC12\BITM\seip107919\Utility\Utility;

$birthday = new Birthday();
$birthday->prepare($_REQUEST)->update();
?>
